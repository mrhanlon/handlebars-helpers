# Handlebars Helpers

This is a collection of various [Handlebars.js](http://handlebarsjs.com/) that
I've written. In particular the need for this arose when using
[jknack](https://github.com/jknack)'s
[Handlebars.java](http://jknack.github.io/handlebars.java) library for using
Handlebars templates on the server side. Quickly, the need for sharing templates
on both the server-side and client-side arose. With most simple templates this
isn't a problem, and Handlebars.java provides very nicely for shipping both raw
templates and precompiled templates to the client. You can also use
[Grunt](http://gruntjs.com/) to precompile. But once you start using non-core
helpers, this becomes a problem.

Handlebars.java has a nice mechanism for registering helpers on the server side,
very similar to Handlebars.js's mechanism client-side. There are also several
optional helpers included in the library, but these are non-standard, as far as
Handlebars.js is concerned, and not included in the JavaScript library. Using
those helpers client-side is not available by default.

## Liferay

Currently included are [Liferay](http://liferay.com) helpers to generate portlet
URLs. Supported are **Render**, **Action**, and **Resource** URL types. URLs are
created using the [Liferay.PortletURL]() JavaScript library, included with
Liferay.

A complimentary server-side helper class is available. **TODO link/reference**
This helper allows templates that use the server-side helper to also be rendered
on the client, but as one can expect, there are some caveats. On the server, a
PortletRequest object is available, from which to generate the URLs. On the
client, the PortletRequest object is not available (does not exist!) We expect a
stand-in object thata fulfills the same role on the client, i.e. providing a
reference to the Liferay Page Layout ID and the Portlet ID. Both of these values
are relatively easy to obtain on the client:

    var request = {
      plid: Liferay.ThemeDisplay.getPlid()
      , portletId: $('.portlet-boundary').attr('id').replace(/p_p_id_(.*)_/, '$1');
    };

Including this "request" object with the context object passed to the helper
provides enough information for the helper to render URLs.

### Usage

We assume the following request object:

    var request = {
      plid: 123 /* this is the plid for the path '/page-layout' */
      , portletId: my_WAR_myportlet
    };

#### Render URL

    {{renderURL request param0="value0" param1="value1"}}

Output

    http://example.com/page-layout?p_p_id=my_WAR_myportlet&p_p_lifecycle=0&_my_WAR_myportlet_param0=value0&_my_WAR_myportlet_param1=value1

#### Action URL

    {{actionURL request param0="value0" param1="value1"}}

Output

    http://example.com/page-layout?p_p_id=my_WAR_myportlet&p_p_lifecycle=1&_my_WAR_myportlet_param0=value0&_my_WAR_myportlet_param1=value1

#### Resource URL

    {{resourceURL request "imageResource123"}}

Output

    http://example.com/page-layout?p_p_id=my_WAR_myportlet&p_p_resource_id=imageResource123

### Special parameters

The following "special" parameter names are supported in the parameter hash:

    windowState

If this parameter is provided, the WindowState will be set on the URL. Acceptable
values are: minimized, maximized, normal, exclusive.

    resourceID

Vaild only for Resource URLs, if this parameter is provided the Resource ID will
be set on the URL.


## JodaTime

This helper provides support for formatting dates client-side. It works in conjunction
with the [JodaHelper](https://github.com/jknack/handlebars.java/blob/master/handlebars-helpers/src/main/java/com/github/jknack/handlebars/helper/JodaHelper.java) the server side.

**TODO: provide support for JodaPattern and JodaISO formatting.**

### Example

    {{jodaStyle created "SS"}}

Output:

    11/15/1980, 2:30 PM


## i18n-js

Support for the [i18n-js](https://github.com/fnando/i18n-js) JavaScript library.
Allows translations to be made client-side. Works in conjunction with the [i18n](https://github.com/jknack/handlebars.java/blob/master/handlebars/src/main/java/com/github/jknack/handlebars/helper/I18nHelper.java#L105)
and [i18nJs](https://github.com/jknack/handlebars.java/blob/master/handlebars/src/main/java/com/github/jknack/handlebars/helper/I18nHelper.java#L193) helpers provided by Handlebars.java.

To use this helper, you must output the translations using the i18nJs helper.
Then you can use the client-side i18n helper to produce translations client-side.

### Example:

    {{i18nJs}}
    ...
    {{i18n "errormessage"}}

Output:

    <script type="text/javascript">
      /* English */
      I18n.translations = I18n.translations || {};
      I18n.translations['en'] = {
        "errormessage": "There was an error."
      };
    </script>
    ...
    There was an error.