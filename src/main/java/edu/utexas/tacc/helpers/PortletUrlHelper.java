/**
 *
 */
package edu.utexas.tacc.helpers;

import java.io.IOException;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.ResourceURL;
import javax.portlet.WindowStateException;

import org.apache.log4j.Logger;

import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.WindowStateFactory;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletURLFactoryUtil;

/**
 * @author mrhanlon
 *
 */
public enum PortletUrlHelper implements Helper<PortletRequest> {

    renderURL {

        @Override
        protected CharSequence safeApply(PortletRequest context, Options options) {
            return createPortletURL(context, options, PortletRequest.RENDER_PHASE).toString();
        }

    },

    actionURL {

        @Override
        protected CharSequence safeApply(PortletRequest context, Options options) {
            return createPortletURL(context, options, PortletRequest.ACTION_PHASE).toString();
        }
    },

    resourceURL {

        @Override
        protected CharSequence safeApply(PortletRequest context, Options options) {
            return createPortletURL(context, options, PortletRequest.RESOURCE_PHASE).toString();
        }
    },

    namespace {

        @Override
        protected CharSequence safeApply(PortletRequest context, Options options) {
            return (String) context.getAttribute(WebKeys.PORTLET_ID);
        }

    };

    private static final Logger logger = Logger.getLogger(PortletUrlHelper.class);

    @Override
    public CharSequence apply(final PortletRequest context, final Options options) throws IOException {
        if (options.isFalsy(context)) {
            Object param = options.param(0, null);
            return param == null ? null : param.toString();
        }
        return safeApply(context, options);
    }

    protected PortletURL createPortletURL(PortletRequest portletRequest, Options options, String lifecycle) {
        ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
        String portletName = (String) portletRequest.getAttribute(WebKeys.PORTLET_ID);
        PortletURL portletURL = PortletURLFactoryUtil.create(PortalUtil.getHttpServletRequest(portletRequest),
                portletName, themeDisplay.getLayout().getPlid(), lifecycle);

        for (String key : options.hash.keySet()) {
            Object value = options.hash(key);

            if (key.equals("resourceID")) {
                if (portletURL instanceof ResourceURL) {
                    ((ResourceURL) portletURL).setResourceID(value.toString());
                }
            }
            else if (key.equals("windowState")) {
                try {
                    ((LiferayPortletURL) portletURL).setWindowState(WindowStateFactory.getWindowState(value.toString()));
                } catch (WindowStateException e) {
                    logger.warn("Undefined windowState: " + value);
                }
            }
            else {
                portletURL.setParameter(key, value.toString());
            }
        }

        return portletURL;
    }

    /**
     * Apply the helper to the context.
     *
     * @param context
     *            The context object (param=0).
     * @param options
     *            The options object.
     * @return A string result.
     */
    protected abstract CharSequence safeApply(final PortletRequest context, final Options options);
}