/*globals moment, Handlebars*/
'use strict';

Handlebars.registerHelper('jodaStyle', function(date, options) {
  var fmtTime, fmtDate;
  switch (options[0]) {
    case 'S':
      fmtTime = 'h:mm A';
      break;
    case 'M':
      fmtTime = 'h:mm:ss A';
      break;
    case 'L':
      fmtTime = 'h:mm:ss A Z'
      break;
    default:
      fmtTime = '';
  }
  switch (options[1]) {
    case 'S':
      fmtDate = 'M/D/YYYY';
      break;
    case 'M':
      fmtDate = 'MMM. D, YYYY';
      break;
    case 'L':
      fmtDate = 'MMMM D, YYYY';
      break;
    default:
      fmtDate = '';
  }
  return moment(date).format(fmtTime ? fmtDate + ', ' + fmtTime : fmtDate);
});
