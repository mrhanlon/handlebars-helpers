/*globals I18n, Handlebars*/
'use strict';
Handlebars.registerHelper('i18n', function(context, options) {
  return I18n.t(context, options.hash);
});