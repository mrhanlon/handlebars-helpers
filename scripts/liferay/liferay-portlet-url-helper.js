/*globals AUI, Liferay, Handlebars*/
'use strict';
AUI().use('liferay-portlet-url', function() {

  Handlebars.registerHelper('renderURL', function(request, options) {
    var url = Liferay.PortletURL.createRenderURL();
    url.setPlid(request.plid);
    url.setPortletId(request.portletId);
    if (typeof options.hash === 'object') {
      for (var key in options.hash) {
        if (options.hash.hasOwnProperty(key)) {
          if (key === 'windowState') {
            url.setWindowState(options.hash[key]);
          } else {
            url.setParameter(key, options.hash[key]);
          }
        }
      }
    }
    return url.toString();
  });

  Handlebars.registerHelper('actionURL', function(request, options) {
    var url = Liferay.PortletURL.createActionURL();
    url.setPlid(request.plid);
    url.setPortletId(request.portletId);
    if (typeof options.hash === 'object') {
      for (var key in options.hash) {
        if (options.hash.hasOwnProperty(key)) {
          if (key === 'windowState') {
            url.setWindowState(options.hash[key]);
          } else {
            url.setParameter(key, options.hash[key]);
          }
        }
      }
    }
    return url.toString();
  });

  Handlebars.registerHelper('resourceURL', function(request, options) {
    var url = Liferay.PortletURL.createResourceURL();
    url.setPlid(request.plid);
    url.setPortletId(request.portletId);

    if (typeof options === 'string') {
      url.setResourceId(options);
    }
    else if (options instanceof String) {
      url.setResourceId(options.toString());
    }
    else if (typeof options.hash === 'object') {
      for (var key in options.hash) {
        if (options.hash.hasOwnProperty(key)) {
          if (key === 'resourceID') {
            url.setResourceId(options.hash[key]);
          } else {
            url.setParameter(key, options.hash[key]);
          }
        }
      }
    }
    return url.toString();
  });

});
